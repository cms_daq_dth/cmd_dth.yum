 sudo yum repo-pkgs DTH-Kit_p1_v2 list  : list packages in repo
 createrepo DTH-Kit_p1_v                : argument is path to repo
 yum clean all                          : clean all metadata
 
sudo yum -v clean expire-cache          : expires caches and shows which it
                                        : really cleans
					: THIS WORKS IF YUM REPO UPDATES
					: yum clean all does NOT do the job
repoquery -i kernel-3.10.0-1062.el7.x86_64: shows in which repo the pkg is.
yum repolist                            : list repos
yum-config-manager --disable updates/7/x86_64
sudo systemctl status packagekit        : show status of packagekit
systemctl stop packagekit
systemctl mask packagekit
