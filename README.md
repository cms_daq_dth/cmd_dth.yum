# CMD_DTH.yum

Yum repositories for DTH related software

## Updating the repository

*ATTENTION*: If the repo is created incorrectly on a recent Redhat-based system
it will create meta info files which are not compatible with the version
of yum in CentOS7. Hence this repo needs to be created and updated on
old redhat OS systems as long as we need to support the P1 CentOS7 system.
Always use the --compatibility flag of the createrepo command.

*More ATTENTION*: The TCDS firmware image RPM is stored in git LFS.

Make sure you have an up to date clone of the master branch checked
out. You need the createrepo utility to update the repository. On
CentOS this is contained in the package "createrepo".

1) Copy the desired new rpms to the directory DTH-Kit. Delete
undesired rpms from this directory (eg. using git rm )

2) In the top directory of the repository run the command
<pre>find DTH-Kit -type d -and \( -name 'base' -or -name 'updates' -or -name
'testing' \) -exec createrepo --compatibility {} \;</pre>
This will update the metainformation contained in
DTH-Kit/repodate. Files in this directory will change names.

3) Include the new metainfo files in the repository, commit, and push:
<pre>
git add --all
git commit -a -m"Update of the repository with a new version of rpms"
git push
</pre>

If you have made an update of the yum repository and you want to test
these updates immediately on a client machine you need to expire the
cache on the client with the command
<pre>sudo yum -v clean expire-cache</pre>
To list available and installed packages of the
repository use:
<pre>yum repo-pkgs DTH-Kit list</pre>
